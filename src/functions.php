<?php
require "vendor/autoload.php";

function getDinos()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/');
    return json_decode($response->body);
}

function getDino($name)
{
    $uri = "https://allosaurus.delahayeyourself.info/api/dinosaurs/$name";
    $response = Requests::get($uri);
    return json_decode($response->body);
}